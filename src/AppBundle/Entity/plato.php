<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plato
 *
 * @ORM\Table(name="plato")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\platoRepository")
 */
class plato
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=25)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredientes", type="text")
     */
    private $ingredientes;

    /**
     * @var int
     *
     * @ORM\Column(name="precio", type="integer")
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="alergenos", type="string", length=100)
     */
    private $alergenos;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_plato", type="string")
     */
    private $tipoPlato;

    /**
     * @var bool
     *
     * @ORM\Column(name="destacado", type="boolean")
     */
    private $destacado;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return plato
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ingredientes
     *
     * @param string $ingredientes
     *
     * @return plato
     */
    public function setIngredientes($ingredientes)
    {
        $this->ingredientes = $ingredientes;

        return $this;
    }

    /**
     * Get ingredientes
     *
     * @return string
     */
    public function getIngredientes()
    {
        return $this->ingredientes;
    }

    /**
     * Set precio
     *
     * @param integer $precio
     *
     * @return plato
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return int
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set alergenos
     *
     * @param string $alergenos
     *
     * @return plato
     */
    public function setAlergenos($alergenos)
    {
        $this->alergenos = $alergenos;

        return $this;
    }

    /**
     * Get alergenos
     *
     * @return string
     */
    public function getAlergenos()
    {
        return $this->alergenos;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return plato
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set tipoPlato
     *
     * @param string $tipoPlato
     *
     * @return plato
     */
    public function setTipoPlato($tipoPlato)
    {
        $this->tipoPlato = $tipoPlato;

        return $this;
    }

    /**
     * Get tipoPlato
     *
     * @return string
     */
    public function getTipoPlato()
    {
        return $this->tipoPlato;
    }

    /**
     * Set destacado
     *
     * @param boolean $destacado
     *
     * @return plato
     */
    public function setDestacado($destacado)
    {
        $this->destacado = $destacado;

        return $this;
    }

    /**
     * Get destacado
     *
     * @return bool
     */
    public function getDestacado()
    {
        return $this->destacado;
    }
}

