<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\plato;
use AppBundle\Entity\Empleado;
use AppBundle\Form\ReservaType;
use AppBundle\Entity\Contacto;
use AppBundle\Form\ContactoType;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        //Mostrar platos destacados en la home
        $plato_repository = $this->getDoctrine()->getRepository(plato::class);
        $plato = $plato_repository->findByDestacado(1);
        //Mostrar empleados en la home
        $repository = $this->getDoctrine()->getRepository(Empleado::class);
        $empleado = $repository->findAll();



        $contacto = new Contacto();
        //CREACION DEL FORMULARIO
        $form_contacto = $this->createForm(ContactoType::class, $contacto);
        //RECOGIDA DE INFORMACION
        $form_contacto->handleRequest($request);
        //COMPROBACION DEL FORMULARIO
        if ($form_contacto->isSubmitted() && $form_contacto->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            //RELLENO DE LA INFORMACION
            $contacto = $form_contacto->getData();
            //ALMACENAMIENTO DE LA INFORMACION
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contacto);
            $entityManager->flush();
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($task);
            // $entityManager->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/index.html.twig', array('platos' => $plato, 'empleado' => $empleado, 'contacto' => $form_contacto->createView()));
    }

    /**
     * @Route("/carta", name="carta")
     */
    public function cartaAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(plato::class);
        $plato = $repository->findAll();
        // replace this example code with whatever you need
        return $this->render('default/carta.html.twig', array('platos' => $plato));
    }
    /**
     * @Route("/nosotros", name="nosotros")
     */
    public function nosotrosAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/nosotros.html.twig');
    }
    /**
     * @Route("/domicilio", name="domicilio")
     */
    public function domicilioAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/domicilio.html.twig');
    }
    /**
     * @Route("/contacto", name="contacto")
     */
    public function contactoAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/contacto.html.twig');
    }


}