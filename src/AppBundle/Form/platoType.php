<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class platoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class)
            ->add('ingredientes', CKEditorType::class)
            ->add('precio', MoneyType::class)
            ->add('alergenos', TextareaType::class, array('required' => false))
            ->add('imagen', FileType::class, array('attr'=>array('onchange'=>'onChange(event)'), 'data_class' => null))
            //->add('tipo_plato', TextType::class)
            ->add('tipo_plato', ChoiceType::class, array(
                'choices'  => array(
                    'Primero' => 'Primero',
                    'Segundo' => 'Segundo',
                    'Postre' => 'Postre',
                    'Bebida' => 'Bebida',
                    'Picoteo' => 'Picoteo',
                ),
            ))
            ->add('destacado', CheckboxType::class, array(
                'label'    => 'Destacar',
                'required' => false,
            ))
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\plato'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_plato';
    }


}

